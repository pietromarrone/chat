# PagoPA: Senior Software Engineer

## Test 1: Write a very simple chat server that should listen on TCP port 10000 for clients. The chat protocol is very simple, clients connect with "telnet" and write single lines of text. On each new line of text, the server will broadcast that line to all other connected clients. Your program should be fully tested too.

This is an Char Server-Client application

## How to use

### Java Run

```
mvn clean package
java -jar target/chat-1.0.jar
```

or run Docker Image

### Docker Image

```
docker run pietromarrone/chat-server -p 10000:10000
```

or build

### Docker Image

```
mvn clean test package jib:dockerBuild
docker run pietromarrone/chat-server -p 10000:10000
```

### Clients

on others shell (one per client)

```
telnet [HOST-ADDRESS] 10000
Hi Client 0, Welcome aboard! type any line and press <ENTER>; type exit to Exit
```

### Continuous Integration

On every commit Gitlab perform these Steps:

- build
- test
- package
- deploy docker image

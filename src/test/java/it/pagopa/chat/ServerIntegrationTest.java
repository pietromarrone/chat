package it.pagopa.chat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.Socket;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.pagopa.chat.base.ClientWrapper;
import it.pagopa.chat.base.ServerWrapper;

class ServerIntegrationTest {

  ServerWrapper server;

  @BeforeEach
  public void setup() throws IOException {
    server = new ServerWrapper();
    new Thread(server).start();
  }

  @AfterEach
  public void tearDown() {
    server.stop();
    sleep();
  }

  @Test
  public void test_StopServer() throws IOException {
    assertTrue(server.isRunning());

    server.stop();
    assertFalse(server.isRunning());
  }

  @Test
  public void test_StopServer_WithConnectedClients() throws IOException {
    assertTrue(server.isRunning());
    openSocketAndWait();
    assertEquals(1, server.getClientsCount());

    server.stop();
    assertFalse(server.isRunning());
    assertEquals(0, server.getClientsCount());
  }

  @Test
  public void test_ClientConnection() throws IOException {
    openSocketAndWait();
    assertEquals(1, server.getClientsCount());
  }

  @Test
  public void test_ClientExit() throws IOException {
    openSocketAndWait();

    ClientHandler client = server.getClients()
        .iterator()
        .next();
    client.exit();
    assertEquals(0, server.getClientsCount());
  }

  @Test
  public void test_ServerBroadcast() throws IOException {
    openSocketAndWait();

    ClientWrapper client = (ClientWrapper) server.getClients()
        .iterator()
        .next();
    assertEquals(1, server.getClientsCount());

    server.broadcast("MOCK", "TEST");

    assertEquals("MOCK> TEST", client.lastMessageReceived);
  }

  private Socket openSocketAndWait() {
    Socket socket;
    try {
      // socket = new Socket(server.serverSocket.getInetAddress(),
      // server.serverSocket.getLocalPort());
      socket = new Socket("localhost", 10000);
      sleep();
      return socket;
    } catch (Exception e) {
    }
    return null;
  }

  private void sleep() {
    try {
      Thread.sleep(3000);
    } catch (Exception e) {
    }
  }

}

package it.pagopa.chat.base;

import java.io.IOException;
import java.net.Socket;

import it.pagopa.chat.Server;

public class ServerWrapper extends Server {

	public ServerWrapper() throws IOException {
		super();
	}

	@Override
	protected ClientWrapper createClient(Socket clientSocket) throws IOException {
		// Create a new handler object for handling this request.
		ClientWrapper client = new ClientWrapper("Client " + System.currentTimeMillis(), this, clientSocket);

		// Create a new Thread with this object.
		// and start the thread.
		new Thread(client, client.getName()).start();

		return client;
	}

}

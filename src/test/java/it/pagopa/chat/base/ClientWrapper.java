package it.pagopa.chat.base;

import java.io.IOException;
import java.net.Socket;

import it.pagopa.chat.ClientHandler;
import it.pagopa.chat.Server;

public class ClientWrapper extends ClientHandler {

	public String lastMessageReceived;

	public ClientWrapper(String name, Server server, Socket socket) throws IOException {
		super(name, server, socket);
	}

	@Override
	public void write(String msg) {
		lastMessageReceived = msg;
	}

}

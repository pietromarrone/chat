package it.pagopa.chat;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * /** The Chat Server
 *
 * @since 1.0
 * @author pietromarrone
 */
public class Server implements Runnable {

  private ServerSocket serverSocket;
  private int clientsCount = 0;
  private boolean isRunning = true;
  private Set<ClientHandler> clients = new HashSet<ClientHandler>();

  /**
   * Start the Server on default port 10000
   *
   * @throws IOException
   */
  public Server() throws IOException {
    this(10000);
  }

  /**
   * Start the Server on specified port
   *
   * @throws IOException
   */
  public Server(int port) throws IOException {
    serverSocket = new ServerSocket(port);
    System.out.println("Chat server is Starting on");
    System.out.println("IP Address: " + InetAddress.getLocalHost()
        .getHostAddress());
  }

  /**
   * Start the Chat Server
   *
   * @see java.lang.Thread#run()
   */
  @Override
  public void run() {
    // running infinite loop for getting client request
    while (isRunning) {
      try {
        // Accept the incoming request
        Socket clientSocket = serverSocket.accept();

        if (isRunning) {
          System.out.println("New client request received : " + clientSocket);
          ClientHandler client = createClient(clientSocket);

          System.out.println("Adding " + client.getName() + " to active client list");
          // add this client to active clients list
          clients.add(client);

          // increment i for new client.
          clientsCount++;
          System.out.println(clientsCount + " active Clients");

          // If is Not Running this Client is From stop() method
        } else {
          System.out.println("Server stopped, exiting now");
          serverSocket.close();
          break;
        }

      } catch (IOException e) {
        System.err.println("Unable to Conntect Client");
      }
    }
  }

  protected ClientHandler createClient(Socket clientSocket) throws IOException {
    // Create a new handler object for handling this request.
    ClientHandler client = new ClientHandler("Client " + clientsCount, this, clientSocket);

    // Create a new Thread with this object.
    // and start the thread.
    new Thread(client, client.getName()).start();

    return client;
  }

  /**
   * Stop the Server:<br>
   * First Disconnect all connected Clients, then require a client connection to
   * unlock Server from accept(), in the end the {@code Server} {@code Thread}
   * exit
   */
  public void stop() {
    if (!isRunning) {
      System.err.println("Chat Server already Stopped");
      return;
    }

    this.isRunning = false;
    System.out.println("Chat server is Stopping...");
    broadcast("Server", "Server is shutting down...");

    try {
      for (ClientHandler client : new ArrayList<ClientHandler>(this.clients)) {
        client.exit();
      }

      new Socket(serverSocket.getInetAddress(), serverSocket.getLocalPort()).close();
    } catch (IOException e) {
      System.err.println("Cannot Stop the Chat Server");
      e.printStackTrace();
    }
  }

  /**
   * Disconnect the Client
   *
   * @param client: the Client to disconnect
   */
  public void disconnect(ClientHandler client) {
    System.out.println("Removing " + client + "...");

    clients.remove(client);
    clientsCount--;

    System.out.println(clientsCount + " active Clients");
  }

  /**
   * Broadcast Message from on {@code Client} to All others
   *
   * @param fromClient the {@code Client} sender
   * @param msg        the message to cast
   */
  public void broadcast(String fromClient, String msg) {
    // Copy client list (don't want to hold lock while doing IO)
    List<ClientHandler> clients = null;
    synchronized (this) {
      clients = new ArrayList<ClientHandler>(this.clients);
    }
    for (ClientHandler client : clients) {
      // Do not send to the Writer
      if (!client.getName()
          .equals(fromClient)) {
        try {
          client.write(fromClient + "> " + msg);
        } catch (Exception e) {
          System.err.println("Cannot Broadcast to Client: " + client);
        }
      }
    }
  }

  public int getClientsCount() {
    return clientsCount;
  }

  public boolean isRunning() {
    return isRunning;
  }

  protected Set<ClientHandler> getClients() {
    return clients;
  }

  public static void main(String[] args) throws IOException, InterruptedException {
    Server server;
    if (args.length > 0) {
      int port = Integer.parseInt(args[0]);
      server = new Server(port);
    } else {
      server = new Server();
    }

    // start the Server
    new Thread(server, "Chat Server").start();
  }
}

package it.pagopa.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.util.Objects;

/**
 * ClientHandler this class Handle the connected Client
 *
 * @author pmarrone
 */
public class ClientHandler implements Runnable {
  private final String name;
  private final Server server;
  protected Socket socket;
  private BufferedReader input;
  private Writer output;

  public ClientHandler(String name, Server server, Socket socket) throws IOException {
    this.name = name;
    this.socket = socket;
    this.server = server;
    this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    this.output = new OutputStreamWriter(socket.getOutputStream());

    write("Hi " + name + ", Welcome aboard! type any line and press <ENTER>; type exit to Exit");
    server.broadcast(name, "Hello, I'm joined the chat");
  }

  public void write(String msg) {
    try {
      output.write(msg + "\r\n");
      output.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public String getName() {
    return name;
  }

  /**
   * Start the Chat Client Handler
   *
   * @see java.lang.Thread#run()
   */
  @Override
  public void run() {
    String line = null;

    try {
      while (true) {
        line = input.readLine();

        if (line.equals("exit")) {
          exit();
          break;
        }

        server.broadcast(name, line);
      }
    } catch (IOException e) {
      System.err.println("Server is Down: " + e.getMessage());
    }
  }

  /**
   * Stop the Client:<br>
   * First Disconnect itself from {@code Server}, then close connections, in the
   * end the {@code ClientHandler} {@code Thread} exit
   */
  public void exit() {
    write("Bye Bye");
    server.broadcast(name, "I'm leaving the chat!");

    server.disconnect(this);
    try {
      if (output != null) {
        output.close();
        output = null;
      }
      if (input != null) {
        input.close();
        input = null;
      }
      if (!socket.isClosed()) {
        socket.close();
        socket = null;
      }
    } catch (IOException e) {
      System.err.println("Unable to Close Socket: " + e.getMessage());
    }
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    return Objects.equals(this.name, ((ClientHandler) obj).getName());
  }

  @Override
  public String toString() {
    return name;
  }
}

#!/bin/sh

echo "The Chat is starting"
exec java ${JAVA_OPTS} -noverify -XX:+AlwaysPreTouch -Djava.security.egd=file:/dev/./urandom -cp /app/resources/:/app/classes/:/app/libs/* "it.pagopa.chat.Server"  "$@"
